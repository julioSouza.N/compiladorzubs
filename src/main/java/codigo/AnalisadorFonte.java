package codigo;

import contexto.ZubsContexto;
import gramatica.ZubsLexer;
import gramatica.ZubsParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;

public class AnalisadorFonte {

    private ZubsContexto listener;

    public void analizar(String fonte) {

        try {

            ZubsLexer lexico = new ZubsLexer(CharStreams.fromFileName(fonte));

            CommonTokenStream token = new CommonTokenStream(lexico);

            ZubsParser sintatico = new ZubsParser(token);

            ZubsParser.ProgramaContext arvore = sintatico.programa();

            ParseTreeWalker verificador = new ParseTreeWalker();

            listener = new ZubsContexto();

            verificador.walk(listener,arvore);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }



}
