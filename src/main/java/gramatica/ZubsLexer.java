// Generated from /home/julio/Faculdade/compiladores/compilador/src/main/java/Zubs.g4 by ANTLR 4.7.2
package gramatica;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ZubsLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		OPERADORES=10, START=11, FIMBLOCO=12, INICIOBLOCO=13, FIMPROGRAMA=14, 
		INICIO_FUNC=15, VOID=16, INT=17, FLOAT=18, CHAR=19, TEXT=20, BOOLEAN=21, 
		AND=22, OR=23, NOT=24, IGUAL=25, DIFERENTE=26, MENOR_QUE=27, MAIOR_QUE=28, 
		MAIOR_IGUAL=29, MENOR_IGUAL=30, SUM=31, SUB=32, MULT=33, DIV=34, RESTO=35, 
		VAL_TEXT=36, NUMERO=37, VAL_FLOAT=38, VAL_BOOLEAN=39, IDENTIFICADO=40, 
		WS=41;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
			"OPERADORES", "START", "FIMBLOCO", "INICIOBLOCO", "FIMPROGRAMA", "INICIO_FUNC", 
			"VOID", "INT", "FLOAT", "CHAR", "TEXT", "BOOLEAN", "AND", "OR", "NOT", 
			"IGUAL", "DIFERENTE", "MENOR_QUE", "MAIOR_QUE", "MAIOR_IGUAL", "MENOR_IGUAL", 
			"SUM", "SUB", "MULT", "DIV", "RESTO", "VAL_TEXT", "NUMERO", "VAL_FLOAT", 
			"VAL_BOOLEAN", "IDENTIFICADO", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'program'", "';'", "'type'", "'('", "')['", "'return'", "','", 
			"'='", "')'", null, "'begin'", "']'", "'['", "'.'", "'function'", "'void'", 
			"'int'", "'float'", "'char'", "'text'", "'boolean'", "'AND'", "'OR'", 
			"'NOT'", "'=='", "'~'", "'<'", "'>'", "'>='", "'<='", "'+'", "'-'", "'*'", 
			"'/'", "'%'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, "OPERADORES", 
			"START", "FIMBLOCO", "INICIOBLOCO", "FIMPROGRAMA", "INICIO_FUNC", "VOID", 
			"INT", "FLOAT", "CHAR", "TEXT", "BOOLEAN", "AND", "OR", "NOT", "IGUAL", 
			"DIFERENTE", "MENOR_QUE", "MAIOR_QUE", "MAIOR_IGUAL", "MENOR_IGUAL", 
			"SUM", "SUB", "MULT", "DIV", "RESTO", "VAL_TEXT", "NUMERO", "VAL_FLOAT", 
			"VAL_BOOLEAN", "IDENTIFICADO", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ZubsLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Zubs.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2+\u0121\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3"+
		"\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25"+
		"\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\34\3\34\3\35"+
		"\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3"+
		"%\3%\7%\u00d5\n%\f%\16%\u00d8\13%\3%\3%\3&\6&\u00dd\n&\r&\16&\u00de\3"+
		"\'\6\'\u00e2\n\'\r\'\16\'\u00e3\3\'\3\'\3\'\5\'\u00e9\n\'\5\'\u00eb\n"+
		"\'\5\'\u00ed\n\'\3(\3(\3(\3(\3(\3(\3(\3(\3(\5(\u00f8\n(\3)\6)\u00fb\n"+
		")\r)\16)\u00fc\3)\7)\u0100\n)\f)\16)\u0103\13)\3*\3*\3*\3*\7*\u0109\n"+
		"*\f*\16*\u010c\13*\3*\3*\3*\3*\3*\3*\7*\u0114\n*\f*\16*\u0117\13*\3*\6"+
		"*\u011a\n*\r*\16*\u011b\5*\u011e\n*\3*\3*\4\u00d6\u010a\2+\3\3\5\4\7\5"+
		"\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23"+
		"%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G"+
		"%I&K\'M(O)Q*S+\3\2\b\6\2\'\',-//\61\61\4\2$$^^\4\2C\\c|\3\2\62;\4\2\f"+
		"\f\17\17\5\2\13\f\17\17\"\"\2\u012e\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2"+
		"\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23"+
		"\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2"+
		"\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2"+
		"\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3"+
		"\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2"+
		"\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2"+
		"\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\3U\3\2\2\2\5]\3\2\2\2\7_\3\2\2\2\td"+
		"\3\2\2\2\13f\3\2\2\2\ri\3\2\2\2\17p\3\2\2\2\21r\3\2\2\2\23t\3\2\2\2\25"+
		"v\3\2\2\2\27x\3\2\2\2\31~\3\2\2\2\33\u0080\3\2\2\2\35\u0082\3\2\2\2\37"+
		"\u0084\3\2\2\2!\u008d\3\2\2\2#\u0092\3\2\2\2%\u0096\3\2\2\2\'\u009c\3"+
		"\2\2\2)\u00a1\3\2\2\2+\u00a6\3\2\2\2-\u00ae\3\2\2\2/\u00b2\3\2\2\2\61"+
		"\u00b5\3\2\2\2\63\u00b9\3\2\2\2\65\u00bc\3\2\2\2\67\u00be\3\2\2\29\u00c0"+
		"\3\2\2\2;\u00c2\3\2\2\2=\u00c5\3\2\2\2?\u00c8\3\2\2\2A\u00ca\3\2\2\2C"+
		"\u00cc\3\2\2\2E\u00ce\3\2\2\2G\u00d0\3\2\2\2I\u00d2\3\2\2\2K\u00dc\3\2"+
		"\2\2M\u00e1\3\2\2\2O\u00f7\3\2\2\2Q\u00fa\3\2\2\2S\u011d\3\2\2\2UV\7r"+
		"\2\2VW\7t\2\2WX\7q\2\2XY\7i\2\2YZ\7t\2\2Z[\7c\2\2[\\\7o\2\2\\\4\3\2\2"+
		"\2]^\7=\2\2^\6\3\2\2\2_`\7v\2\2`a\7{\2\2ab\7r\2\2bc\7g\2\2c\b\3\2\2\2"+
		"de\7*\2\2e\n\3\2\2\2fg\7+\2\2gh\7]\2\2h\f\3\2\2\2ij\7t\2\2jk\7g\2\2kl"+
		"\7v\2\2lm\7w\2\2mn\7t\2\2no\7p\2\2o\16\3\2\2\2pq\7.\2\2q\20\3\2\2\2rs"+
		"\7?\2\2s\22\3\2\2\2tu\7+\2\2u\24\3\2\2\2vw\t\2\2\2w\26\3\2\2\2xy\7d\2"+
		"\2yz\7g\2\2z{\7i\2\2{|\7k\2\2|}\7p\2\2}\30\3\2\2\2~\177\7_\2\2\177\32"+
		"\3\2\2\2\u0080\u0081\7]\2\2\u0081\34\3\2\2\2\u0082\u0083\7\60\2\2\u0083"+
		"\36\3\2\2\2\u0084\u0085\7h\2\2\u0085\u0086\7w\2\2\u0086\u0087\7p\2\2\u0087"+
		"\u0088\7e\2\2\u0088\u0089\7v\2\2\u0089\u008a\7k\2\2\u008a\u008b\7q\2\2"+
		"\u008b\u008c\7p\2\2\u008c \3\2\2\2\u008d\u008e\7x\2\2\u008e\u008f\7q\2"+
		"\2\u008f\u0090\7k\2\2\u0090\u0091\7f\2\2\u0091\"\3\2\2\2\u0092\u0093\7"+
		"k\2\2\u0093\u0094\7p\2\2\u0094\u0095\7v\2\2\u0095$\3\2\2\2\u0096\u0097"+
		"\7h\2\2\u0097\u0098\7n\2\2\u0098\u0099\7q\2\2\u0099\u009a\7c\2\2\u009a"+
		"\u009b\7v\2\2\u009b&\3\2\2\2\u009c\u009d\7e\2\2\u009d\u009e\7j\2\2\u009e"+
		"\u009f\7c\2\2\u009f\u00a0\7t\2\2\u00a0(\3\2\2\2\u00a1\u00a2\7v\2\2\u00a2"+
		"\u00a3\7g\2\2\u00a3\u00a4\7z\2\2\u00a4\u00a5\7v\2\2\u00a5*\3\2\2\2\u00a6"+
		"\u00a7\7d\2\2\u00a7\u00a8\7q\2\2\u00a8\u00a9\7q\2\2\u00a9\u00aa\7n\2\2"+
		"\u00aa\u00ab\7g\2\2\u00ab\u00ac\7c\2\2\u00ac\u00ad\7p\2\2\u00ad,\3\2\2"+
		"\2\u00ae\u00af\7C\2\2\u00af\u00b0\7P\2\2\u00b0\u00b1\7F\2\2\u00b1.\3\2"+
		"\2\2\u00b2\u00b3\7Q\2\2\u00b3\u00b4\7T\2\2\u00b4\60\3\2\2\2\u00b5\u00b6"+
		"\7P\2\2\u00b6\u00b7\7Q\2\2\u00b7\u00b8\7V\2\2\u00b8\62\3\2\2\2\u00b9\u00ba"+
		"\7?\2\2\u00ba\u00bb\7?\2\2\u00bb\64\3\2\2\2\u00bc\u00bd\7\u0080\2\2\u00bd"+
		"\66\3\2\2\2\u00be\u00bf\7>\2\2\u00bf8\3\2\2\2\u00c0\u00c1\7@\2\2\u00c1"+
		":\3\2\2\2\u00c2\u00c3\7@\2\2\u00c3\u00c4\7?\2\2\u00c4<\3\2\2\2\u00c5\u00c6"+
		"\7>\2\2\u00c6\u00c7\7?\2\2\u00c7>\3\2\2\2\u00c8\u00c9\7-\2\2\u00c9@\3"+
		"\2\2\2\u00ca\u00cb\7/\2\2\u00cbB\3\2\2\2\u00cc\u00cd\7,\2\2\u00cdD\3\2"+
		"\2\2\u00ce\u00cf\7\61\2\2\u00cfF\3\2\2\2\u00d0\u00d1\7\'\2\2\u00d1H\3"+
		"\2\2\2\u00d2\u00d6\t\3\2\2\u00d3\u00d5\13\2\2\2\u00d4\u00d3\3\2\2\2\u00d5"+
		"\u00d8\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00d9\3\2"+
		"\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00da\t\3\2\2\u00daJ\3\2\2\2\u00db\u00dd"+
		"\4\62;\2\u00dc\u00db\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00dc\3\2\2\2\u00de"+
		"\u00df\3\2\2\2\u00dfL\3\2\2\2\u00e0\u00e2\4\62;\2\u00e1\u00e0\3\2\2\2"+
		"\u00e2\u00e3\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00ec"+
		"\3\2\2\2\u00e5\u00ea\7\60\2\2\u00e6\u00e8\4\62;\2\u00e7\u00e9\4\62;\2"+
		"\u00e8\u00e7\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00eb\3\2\2\2\u00ea\u00e6"+
		"\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00ed\3\2\2\2\u00ec\u00e5\3\2\2\2\u00ec"+
		"\u00ed\3\2\2\2\u00edN\3\2\2\2\u00ee\u00ef\7v\2\2\u00ef\u00f0\7t\2\2\u00f0"+
		"\u00f1\7w\2\2\u00f1\u00f8\7g\2\2\u00f2\u00f3\7h\2\2\u00f3\u00f4\7c\2\2"+
		"\u00f4\u00f5\7n\2\2\u00f5\u00f6\7u\2\2\u00f6\u00f8\7g\2\2\u00f7\u00ee"+
		"\3\2\2\2\u00f7\u00f2\3\2\2\2\u00f8P\3\2\2\2\u00f9\u00fb\t\4\2\2\u00fa"+
		"\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2"+
		"\2\2\u00fd\u0101\3\2\2\2\u00fe\u0100\t\5\2\2\u00ff\u00fe\3\2\2\2\u0100"+
		"\u0103\3\2\2\2\u0101\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102R\3\2\2\2"+
		"\u0103\u0101\3\2\2\2\u0104\u0105\7\61\2\2\u0105\u0106\7,\2\2\u0106\u010a"+
		"\3\2\2\2\u0107\u0109\13\2\2\2\u0108\u0107\3\2\2\2\u0109\u010c\3\2\2\2"+
		"\u010a\u010b\3\2\2\2\u010a\u0108\3\2\2\2\u010b\u010d\3\2\2\2\u010c\u010a"+
		"\3\2\2\2\u010d\u010e\7,\2\2\u010e\u011e\7\61\2\2\u010f\u0110\7\61\2\2"+
		"\u0110\u0111\7\61\2\2\u0111\u0115\3\2\2\2\u0112\u0114\n\6\2\2\u0113\u0112"+
		"\3\2\2\2\u0114\u0117\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116"+
		"\u011e\3\2\2\2\u0117\u0115\3\2\2\2\u0118\u011a\t\7\2\2\u0119\u0118\3\2"+
		"\2\2\u011a\u011b\3\2\2\2\u011b\u0119\3\2\2\2\u011b\u011c\3\2\2\2\u011c"+
		"\u011e\3\2\2\2\u011d\u0104\3\2\2\2\u011d\u010f\3\2\2\2\u011d\u0119\3\2"+
		"\2\2\u011e\u011f\3\2\2\2\u011f\u0120\b*\2\2\u0120T\3\2\2\2\20\2\u00d6"+
		"\u00de\u00e3\u00e8\u00ea\u00ec\u00f7\u00fc\u0101\u010a\u0115\u011b\u011d"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}