// Generated from /home/julio/Faculdade/compiladores/compilador/src/main/java/Zubs.g4 by ANTLR 4.7.2
package gramatica;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ZubsParser}.
 */
public interface ZubsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ZubsParser#programa}.
	 * @param ctx the parse tree
	 */
	void enterPrograma(ZubsParser.ProgramaContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#programa}.
	 * @param ctx the parse tree
	 */
	void exitPrograma(ZubsParser.ProgramaContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#programaInit}.
	 * @param ctx the parse tree
	 */
	void enterProgramaInit(ZubsParser.ProgramaInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#programaInit}.
	 * @param ctx the parse tree
	 */
	void exitProgramaInit(ZubsParser.ProgramaInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#bloco}.
	 * @param ctx the parse tree
	 */
	void enterBloco(ZubsParser.BlocoContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#bloco}.
	 * @param ctx the parse tree
	 */
	void exitBloco(ZubsParser.BlocoContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#funcaoPrincipal}.
	 * @param ctx the parse tree
	 */
	void enterFuncaoPrincipal(ZubsParser.FuncaoPrincipalContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#funcaoPrincipal}.
	 * @param ctx the parse tree
	 */
	void exitFuncaoPrincipal(ZubsParser.FuncaoPrincipalContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(ZubsParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(ZubsParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#comandos}.
	 * @param ctx the parse tree
	 */
	void enterComandos(ZubsParser.ComandosContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#comandos}.
	 * @param ctx the parse tree
	 */
	void exitComandos(ZubsParser.ComandosContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#funcao}.
	 * @param ctx the parse tree
	 */
	void enterFuncao(ZubsParser.FuncaoContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#funcao}.
	 * @param ctx the parse tree
	 */
	void exitFuncao(ZubsParser.FuncaoContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#tipoFunc}.
	 * @param ctx the parse tree
	 */
	void enterTipoFunc(ZubsParser.TipoFuncContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#tipoFunc}.
	 * @param ctx the parse tree
	 */
	void exitTipoFunc(ZubsParser.TipoFuncContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#paramFunc}.
	 * @param ctx the parse tree
	 */
	void enterParamFunc(ZubsParser.ParamFuncContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#paramFunc}.
	 * @param ctx the parse tree
	 */
	void exitParamFunc(ZubsParser.ParamFuncContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#defNewVar}.
	 * @param ctx the parse tree
	 */
	void enterDefNewVar(ZubsParser.DefNewVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#defNewVar}.
	 * @param ctx the parse tree
	 */
	void exitDefNewVar(ZubsParser.DefNewVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#defVar}.
	 * @param ctx the parse tree
	 */
	void enterDefVar(ZubsParser.DefVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#defVar}.
	 * @param ctx the parse tree
	 */
	void exitDefVar(ZubsParser.DefVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#tipoVar}.
	 * @param ctx the parse tree
	 */
	void enterTipoVar(ZubsParser.TipoVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#tipoVar}.
	 * @param ctx the parse tree
	 */
	void exitTipoVar(ZubsParser.TipoVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#typeNome}.
	 * @param ctx the parse tree
	 */
	void enterTypeNome(ZubsParser.TypeNomeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#typeNome}.
	 * @param ctx the parse tree
	 */
	void exitTypeNome(ZubsParser.TypeNomeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#valores}.
	 * @param ctx the parse tree
	 */
	void enterValores(ZubsParser.ValoresContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#valores}.
	 * @param ctx the parse tree
	 */
	void exitValores(ZubsParser.ValoresContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#chamarFunc}.
	 * @param ctx the parse tree
	 */
	void enterChamarFunc(ZubsParser.ChamarFuncContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#chamarFunc}.
	 * @param ctx the parse tree
	 */
	void exitChamarFunc(ZubsParser.ChamarFuncContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#paramChamaVar}.
	 * @param ctx the parse tree
	 */
	void enterParamChamaVar(ZubsParser.ParamChamaVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#paramChamaVar}.
	 * @param ctx the parse tree
	 */
	void exitParamChamaVar(ZubsParser.ParamChamaVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#chamaVar}.
	 * @param ctx the parse tree
	 */
	void enterChamaVar(ZubsParser.ChamaVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#chamaVar}.
	 * @param ctx the parse tree
	 */
	void exitChamaVar(ZubsParser.ChamaVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#atribuicoesComando}.
	 * @param ctx the parse tree
	 */
	void enterAtribuicoesComando(ZubsParser.AtribuicoesComandoContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#atribuicoesComando}.
	 * @param ctx the parse tree
	 */
	void exitAtribuicoesComando(ZubsParser.AtribuicoesComandoContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#atribuicoes}.
	 * @param ctx the parse tree
	 */
	void enterAtribuicoes(ZubsParser.AtribuicoesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#atribuicoes}.
	 * @param ctx the parse tree
	 */
	void exitAtribuicoes(ZubsParser.AtribuicoesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#operacoes}.
	 * @param ctx the parse tree
	 */
	void enterOperacoes(ZubsParser.OperacoesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#operacoes}.
	 * @param ctx the parse tree
	 */
	void exitOperacoes(ZubsParser.OperacoesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ZubsParser#operando}.
	 * @param ctx the parse tree
	 */
	void enterOperando(ZubsParser.OperandoContext ctx);
	/**
	 * Exit a parse tree produced by {@link ZubsParser#operando}.
	 * @param ctx the parse tree
	 */
	void exitOperando(ZubsParser.OperandoContext ctx);
}