// Generated from /home/julio/Faculdade/compiladores/compilador/src/main/java/Zubs.g4 by ANTLR 4.7.2
package gramatica;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ZubsParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ZubsVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ZubsParser#programa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrograma(ZubsParser.ProgramaContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#programaInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramaInit(ZubsParser.ProgramaInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#bloco}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloco(ZubsParser.BlocoContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#funcaoPrincipal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncaoPrincipal(ZubsParser.FuncaoPrincipalContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(ZubsParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#comandos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComandos(ZubsParser.ComandosContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#funcao}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncao(ZubsParser.FuncaoContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#tipoFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoFunc(ZubsParser.TipoFuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#paramFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamFunc(ZubsParser.ParamFuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#defNewVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefNewVar(ZubsParser.DefNewVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#defVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefVar(ZubsParser.DefVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#tipoVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipoVar(ZubsParser.TipoVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#typeNome}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeNome(ZubsParser.TypeNomeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#valores}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValores(ZubsParser.ValoresContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#chamarFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChamarFunc(ZubsParser.ChamarFuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#paramChamaVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamChamaVar(ZubsParser.ParamChamaVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#chamaVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChamaVar(ZubsParser.ChamaVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#atribuicoesComando}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtribuicoesComando(ZubsParser.AtribuicoesComandoContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#atribuicoes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtribuicoes(ZubsParser.AtribuicoesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#operacoes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperacoes(ZubsParser.OperacoesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ZubsParser#operando}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperando(ZubsParser.OperandoContext ctx);
}