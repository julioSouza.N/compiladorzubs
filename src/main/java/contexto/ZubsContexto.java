package contexto;

import modelo.Programa;
import gramatica.ZubsBaseListener;
import gramatica.ZubsParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class ZubsContexto extends ZubsBaseListener {

    Programa programa;


/*

    ParserRuleContext parent = parserRuleContext.getParent();

    List<PnpParser.VariableAssignmentContext> ruleContexts = Collections.emptyList();

        while (ruleContexts.isEmpty()) {

        ruleContexts = parent.getRuleContexts(PnpParser.VariableAssignmentContext.class);

        parent = parent.getParent();

    }

        return ruleContexts.get(0);

*/


    @Override
    public void enterPrograma(ZubsParser.ProgramaContext ctx) {

        programa = new Programa();
        super.enterPrograma(ctx);
    }

    @Override
    public void exitPrograma(ZubsParser.ProgramaContext ctx) {
        super.exitPrograma(ctx);
    }

    @Override
    public void enterProgramaInit(ZubsParser.ProgramaInitContext ctx) {

        System.out.println("Um programa com nome = "+ ctx.IDENTIFICADO().getText());

        super.enterProgramaInit(ctx);
    }

    @Override
    public void exitProgramaInit(ZubsParser.ProgramaInitContext ctx) {
        super.exitProgramaInit(ctx);
    }

    @Override
    public void enterBloco(ZubsParser.BlocoContext ctx) {

        super.enterBloco(ctx);
    }

    @Override
    public void exitBloco(ZubsParser.BlocoContext ctx) {
            super.exitBloco(ctx);
    }

    @Override
    public void enterFuncaoPrincipal(ZubsParser.FuncaoPrincipalContext ctx) {

        System.out.println("Esse 'e o bloco do seu programa = "+ ctx.getText());


        super.enterFuncaoPrincipal(ctx);
    }

    @Override
    public void exitFuncaoPrincipal(ZubsParser.FuncaoPrincipalContext ctx) {
        super.exitFuncaoPrincipal(ctx);
    }

    @Override
    public void enterType(ZubsParser.TypeContext ctx) {

        System.out.println(" Tipo estruturado "+ ctx.getText());

        super.enterType(ctx);
    }

    @Override
    public void exitType(ZubsParser.TypeContext ctx) {
        super.exitType(ctx);
    }

    @Override
    public void enterComandos(ZubsParser.ComandosContext ctx) {
        super.enterComandos(ctx);
    }

    @Override
    public void exitComandos(ZubsParser.ComandosContext ctx) {
        super.exitComandos(ctx);
    }

    @Override
    public void enterFuncao(ZubsParser.FuncaoContext ctx) {

        System.out.println("Essa e sua funcao = "+ctx.IDENTIFICADO().getText());
        super.enterFuncao(ctx);
    }

    @Override
    public void exitFuncao(ZubsParser.FuncaoContext ctx) {
        super.exitFuncao(ctx);
    }

    @Override
    public void enterTipoFunc(ZubsParser.TipoFuncContext ctx) {
        System.out.println(ctx.getText());
        super.enterTipoFunc(ctx);
    }

    @Override
    public void exitTipoFunc(ZubsParser.TipoFuncContext ctx) {
        super.exitTipoFunc(ctx);
    }

    @Override
    public void enterParamFunc(ZubsParser.ParamFuncContext ctx) {
        //System.out.println(ctx.getText());
        super.enterParamFunc(ctx);
    }

    @Override
    public void exitParamFunc(ZubsParser.ParamFuncContext ctx) {
        super.exitParamFunc(ctx);
    }

    @Override
    public void enterDefNewVar(ZubsParser.DefNewVarContext ctx) {
      //  System.out.println(ctx.getText());

        super.enterDefNewVar(ctx);
    }

    @Override
    public void exitDefNewVar(ZubsParser.DefNewVarContext ctx) {
        super.exitDefNewVar(ctx);
    }

    @Override
    public void enterDefVar(ZubsParser.DefVarContext ctx) {

      //  System.out.println(ctx.getText());

        super.enterDefVar(ctx);
    }

    @Override
    public void exitDefVar(ZubsParser.DefVarContext ctx) {
        super.exitDefVar(ctx);
    }

    @Override
    public void enterTipoVar(ZubsParser.TipoVarContext ctx) {

        System.out.println(ctx.getText());


        super.enterTipoVar(ctx);
    }

    @Override
    public void exitTipoVar(ZubsParser.TipoVarContext ctx) {
        super.exitTipoVar(ctx);
    }

    @Override
    public void enterTypeNome(ZubsParser.TypeNomeContext ctx) {
        super.enterTypeNome(ctx);
    }

    @Override
    public void exitTypeNome(ZubsParser.TypeNomeContext ctx) {
        super.exitTypeNome(ctx);
    }

    @Override
    public void enterValores(ZubsParser.ValoresContext ctx) {

        super.enterValores(ctx);
    }

    @Override
    public void exitValores(ZubsParser.ValoresContext ctx) {
        super.exitValores(ctx);
    }

    @Override
    public void enterChamarFunc(ZubsParser.ChamarFuncContext ctx) {
        super.enterChamarFunc(ctx);
    }

    @Override
    public void exitChamarFunc(ZubsParser.ChamarFuncContext ctx) {
        super.exitChamarFunc(ctx);
    }

    @Override
    public void enterParamChamaVar(ZubsParser.ParamChamaVarContext ctx) {
        super.enterParamChamaVar(ctx);
    }

    @Override
    public void exitParamChamaVar(ZubsParser.ParamChamaVarContext ctx) {
        super.exitParamChamaVar(ctx);
    }

    @Override
    public void enterChamaVar(ZubsParser.ChamaVarContext ctx) {
        super.enterChamaVar(ctx);
    }

    @Override
    public void exitChamaVar(ZubsParser.ChamaVarContext ctx) {
        super.exitChamaVar(ctx);
    }

    @Override
    public void enterAtribuicoesComando(ZubsParser.AtribuicoesComandoContext ctx) {
        super.enterAtribuicoesComando(ctx);
    }

    @Override
    public void exitAtribuicoesComando(ZubsParser.AtribuicoesComandoContext ctx) {
        super.exitAtribuicoesComando(ctx);
    }

    @Override
    public void enterAtribuicoes(ZubsParser.AtribuicoesContext ctx) {
        super.enterAtribuicoes(ctx);
    }

    @Override
    public void exitAtribuicoes(ZubsParser.AtribuicoesContext ctx) {
        super.exitAtribuicoes(ctx);
    }

    @Override
    public void enterOperacoes(ZubsParser.OperacoesContext ctx) {
        super.enterOperacoes(ctx);
    }

    @Override
    public void exitOperacoes(ZubsParser.OperacoesContext ctx) {
        super.exitOperacoes(ctx);
    }

    @Override
    public void enterOperando(ZubsParser.OperandoContext ctx) {
        super.enterOperando(ctx);
    }

    @Override
    public void exitOperando(ZubsParser.OperandoContext ctx) {
        super.exitOperando(ctx);
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        super.visitErrorNode(node);
    }
}
