grammar Zubs;
programa:  programaInit bloco FIMPROGRAMA;

programaInit: 'program' IDENTIFICADO ';';

bloco: (type|defVar|funcao)* funcaoPrincipal?;

funcaoPrincipal: START  INICIOBLOCO comandos+ FIMBLOCO;
type: 'type' typeNome INICIOBLOCO (defVar | atribuicoesComando)+ FIMBLOCO;
comandos:(defVar | chamarFunc ';' | atribuicoesComando)+;

funcao: INICIO_FUNC tipoFunc IDENTIFICADO '(' paramFunc ')[' comandos ('return' chamaVar ';')? ']';
tipoFunc:INT | FLOAT | BOOLEAN | CHAR | VOID;
paramFunc:((defVar|','paramFunc)*);


defNewVar: (tipoVar (IDENTIFICADO| typeNome) (','(IDENTIFICADO| typeNome))*);
defVar: (defNewVar| defNewVar';') ;


tipoVar: INT | FLOAT | BOOLEAN | CHAR | TEXT | typeNome;

typeNome: IDENTIFICADO;

valores: 
    VAL_TEXT | NUMERO | VAL_FLOAT | VAL_BOOLEAN;

chamarFunc: 
    (IDENTIFICADO '=')* IDENTIFICADO '('  paramChamaVar ')';

paramChamaVar:
    chamaVar?;


chamaVar:
    IDENTIFICADO 
    | chamaVar(',' (IDENTIFICADO|valores))+
    | valores 
    | operacoes
    ;


//atribuiçoes
atribuicoesComando: (IDENTIFICADO|defVar)atribuicoes ';';
atribuicoes: '=' (NUMERO | VAL_BOOLEAN | VAL_TEXT| VAL_FLOAT| chamarFunc | operacoes);
operacoes:
    ((NUMERO|IDENTIFICADO|VAL_FLOAT)
    (OPERADORES(NUMERO|IDENTIFICADO|VAL_FLOAT)+)*)
    |'(' operando OPERADORES operando ')' operacoes*
    |OPERADORES operando ;

operando: (NUMERO|IDENTIFICADO|VAL_FLOAT);

OPERADORES: '+'|'-'|'*'|'/'|'%';


START: 'begin';


FIMBLOCO: ']';
INICIOBLOCO: '[';
FIMPROGRAMA: '.';
INICIO_FUNC: 'function'; 

VOID: 'void';

INT: 'int';
FLOAT: 'float';
CHAR: 'char';
TEXT: 'text';
BOOLEAN: 'boolean';



//funçoes logicas
AND: 'AND';
OR: 'OR';
NOT: 'NOT';

//operador de igualdade
IGUAL: '==';
DIFERENTE: '~';

//operadores relacionais
MENOR_QUE: '<';
MAIOR_QUE: '>';
MAIOR_IGUAL: '>=';
MENOR_IGUAL: '<=';

//expressoes aritmeticas
SUM: '+';
SUB: '-';
MULT: '*';
DIV: '/';
RESTO: '%';



VAL_TEXT: [\\"] .*? [\\"];
NUMERO: ('0'..'9')+;
VAL_FLOAT: ('0'..'9')+ ('.' ('0'..'9' ('0'..'9')?)?)?; 
VAL_BOOLEAN: 'true' | 'false';



IDENTIFICADO:[a-zA-Z]+[0-9]*;

WS : ('/*' .*? '*/'|'//' ~[\r\n]*|[ \t\r\n]+ )-> skip ;