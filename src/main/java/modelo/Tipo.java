package modelo;

import java.util.Arrays;

public enum  Tipo {

	INT ("int"),
	FLOAT ("float"),
	CHAR ("char"),
	TEXT("text"),
	BOOLEAN ("boolean");


	private String valor;

	Tipo(String valor) {
		this.valor = valor;
	}

	public static Tipo construir(String tipoVariavel) {

		return Arrays.stream(Tipo.values())
				.filter(tipo -> tipo.getValor().equals(tipoVariavel))
				.findFirst()
				.orElseThrow(() -> new RuntimeException("TIpo não encontrado"));

	}


	public String getValor() {
		return valor;
	}
}
